package com.deveducation.www.hw3;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


public class Test_SymbolsStrings_Koval {
    String [] myArray = {"bound", "кошка", "собака", "телефон", "клавиатура", "дом", "код", "кодировка", "ручка"};
    String strRus = "Если бы люди понимали самих себя, то больше, чем горами, больше, чем всеми чудесами и красотами мира, они были бы поражены своей способностью мыслить";
    String strEng = "Everything you can imagine is real.";
    static SymbolsStrings_Koval symbols;

    @BeforeAll
    public static void init() {
        symbols = new SymbolsStrings_Koval();
    }

    @Test

    public void test_getEnglishAlphabetCapital() {
        char[] expected = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
        char[] actual = symbols.getEnglishAlphabetCapital();
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test

    public void test_getEnglishAlphabetSmall() {
        char[] expected = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        char[] actual = symbols.getEnglishAlphabetSmall();
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test

    public void test_getRussianAlphabetSmall() {
        char[] expected = {'а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'};
        char[] actual = symbols.getRussianAlphabetSmall();
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test

    public void test_getListOfNumbers() {
        char[] expected = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        char[] actual = symbols.getListOfNumbers();
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test

    public void test_getASCIISymbols() {
        char[] expected = {'!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~'};
        char[] actual = symbols.getASCIISymbols();
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test

    public void test_getIntToString() {
        String expected = "10 is your integer";
        String actual = symbols.getIntToString(10);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @CsvSource(value = {"10 is your integer:10", "999 is your integer:999", "-3 is your integer:-3"}, delimiter = ':')
    void test_getWordsInNumber(String expected, int actual) {
        Assertions.assertEquals(expected, symbols.getIntToString(actual));
    }

    @ParameterizedTest
    @CsvSource(value = {"0.654 is your double:0.654", "8011.000341 is your double:8011.000341", "-305.0008 is your double:-305.0008"}, delimiter = ':')
    void test_getWordsInNumber(String expected, double actual) {
        Assertions.assertEquals(expected, symbols.getDoubleToString(actual));
    }

    @ParameterizedTest
    @CsvSource(value = {"175:75", "100:0", "-1:-101"}, delimiter = ':')
    void test_getWordsInNumber(int expected, String actual) {
        Assertions.assertEquals(expected, symbols.getStringToInt(actual));
    }

    @ParameterizedTest
    @CsvSource(value = {"1.5511:1.3", "-10.9999:-11.251", "0.2511:0.0"}, delimiter = ':')
    void test_getWordsInNumber(double expected, String actual) {
        Assertions.assertEquals(expected, symbols.getStringToDouble(actual));
    }

    @ParameterizedTest
    @CsvSource(value = {"1:Если бы люди понимали самих себя, то больше, чем горами, больше, чем всеми чудесами и красотами мира, они были бы поражены своей способностью мыслить.", "2:Everything you can imagine is real."}, delimiter = ':')
    void test_getLengthOfShortest(int expected, String actual) {
        Assertions.assertEquals(expected, symbols.getLengthOfShortest(actual));
    }

    @Test

    public void test_addSymbolToWords() {
        String [] expected = {"bo$$$", "ко$$$", "собака", "телефон", "клавиатура", "дом", "код", "кодировка", "ру$$$"};
        String [] actual = symbols.addSymbolToWords(this.myArray, 5);
         Assertions.assertArrayEquals(expected, actual);
    }

    @ParameterizedTest
    @CsvSource(value = {"Понадобится мое мнение - я скажу: настоящий друг (именно друг, а не просто приятель) познается в радости; умение радоваться за другого? Редкое качество, очень редкое в наши дни.^Понадобится мое мнение-я скажу:настоящий друг(именно друг, а не просто приятель)познается в радости;умение радоваться за другого?Редкое качество,очень редкое в наши дни.", "In everyday speech, a phrase is any group of words? Often carrying a special idiomatic meaning; in this sense it is synonymous: with expression.^In everyday speech, a phrase is any group of words?Often carrying a special idiomatic meaning; in this sense it is synonymous:with expression."}, delimiter = '^')
    void test_getWordsInNumber(String expected, String actual) {
        Assertions.assertEquals(expected, symbols.addSpaceToPunctuationMarks(actual));
    }


    @ParameterizedTest
    @CsvSource(value = {"Понадбится ме-кжу:щйрг(,пль)зв;?Рчш.^Понадобится мое мнение - я скажу: настоящий друг (именно друг, а не просто приятель) познается в радости; умение радоваться за другого? Редкое качество, очень редкое в наши дни.", "In evrydaspch,igoufw?Otlm;:x.^In everyday speech, a phrase is any group of words? Often carrying a special idiomatic meaning; in this sense it is synonymous: with expression."}, delimiter = '^')
    void test_getUniqueChar(String expected, String actual) {
        Assertions.assertEquals(expected, symbols.getUniqueChar(actual));
    }

    @ParameterizedTest
    @CsvSource(value = {"4^How was your day?", "24^Если бы люди понимали самих себя, то больше, чем горами, больше, чем всеми чудесами и красотами мира, они были бы поражены своей способностью мыслить.", "3^1 plus one"}, delimiter = '^')
    void test_getQuantityOfWords(int expected, String actual) {
        Assertions.assertEquals(expected, symbols.getQuantityOfWords(actual));
    }


    @Test

    public void test_deletePartOfStringRus() {
        String expected = "Если бы люди понимали самих себя, то больше, чем горами, больше, чем всеми чудесами и красотами мира, они были бы поражены своей способностью";
        String actual = symbols.deletePartOfString(strRus, 141, 149);

        Assertions.assertEquals(expected, actual);
    }

    @Test

    public void test_deletePartOfStringEng() {
        String expected = "Everything you imagine is real.";
        String actual = symbols.deletePartOfString(strEng, 15, 19);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @CsvSource(value = {".окшоко ан алес акшоК:Кошка села на окошко.", "niatirB taerG fo latipac eht si nodnoL:London is the capital of Great Britain"}, delimiter = ':')
    void test_getReversedString(String expected, String actual) {
        Assertions.assertEquals(expected, symbols.getReversedString(actual));
    }

    @ParameterizedTest
    @CsvSource(value = {"Кошка села на:Кошка села на окошко", "London is the capital of Great:London is the capital of Great Britain"}, delimiter = ':')
    void test_deleteLastWord(String expected, String actual) {
        Assertions.assertEquals(expected, symbols.deleteLastWord(actual));
    }
    }

